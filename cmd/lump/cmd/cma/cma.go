package cma

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/mvenezia/lump/cmd/lump/cmd"
)

var CMACmd = generateCMACmd()

func init() {
	cmd.RootCmd.AddCommand(CMACmd)

	CMACmd.PersistentFlags().String("aws-region", "us-east-1", "The AWS region")
	CMACmd.PersistentFlags().String("aws-access-key-id", "", "The AWS Access Key ID")
	CMACmd.PersistentFlags().String("aws-secret-access-key", "", "The AWS Secret Access Key")

	viper.BindPFlag("aws-region", CMACmd.PersistentFlags().Lookup("aws-region"))
	viper.BindPFlag("aws-access-key-id", CMACmd.PersistentFlags().Lookup("aws-access-key-id"))
	viper.BindPFlag("aws-secret-access-key", CMACmd.PersistentFlags().Lookup("aws-secret-access-key"))

}

func generateCMACmd() *cobra.Command {
	var createCMACmd = &cobra.Command{
		Use:              "cma",
		Short:            "CMA commands",
		Long:             `CMA Commands`,
		TraverseChildren: true,
	}

	return createCMACmd
}
