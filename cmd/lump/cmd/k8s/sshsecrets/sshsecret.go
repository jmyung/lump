package sshsecrets

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd/k8s"
)

func init() {
	k8s.K8sCmd.AddCommand(SSHSecretCmd)
}

var SSHSecretCmd = generateSSHSecretCmd()

func generateSSHSecretCmd() *cobra.Command {
	var createSSHSecretCmd = &cobra.Command{
		Use:   "ssh-secret",
		Short: "K8S ssh secret commands",
		Long:  `Kubernetes ssh secret Commands`,
	}

	return createSSHSecretCmd
}
