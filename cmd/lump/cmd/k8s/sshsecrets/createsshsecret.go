package sshsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
	"io/ioutil"
	corev1 "k8s.io/api/core/v1"
)

func init() {
	SSHSecretCmd.AddCommand(generateCreateSSHSecretCmd())
}

func generateCreateSSHSecretCmd() *cobra.Command {
	var generateCreateSSHSecretCmd = &cobra.Command{
		Use:   "create [secretName] [privateKeyFile]",
		Short: "Creates a " + string(corev1.SecretTypeSSHAuth) + " secret",
		Long:  "Creates a Kubernetes secret of type " + string(corev1.SecretTypeSSHAuth),
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			createSSHSecret(args)
		},
	}

	return generateCreateSSHSecretCmd
}

func createSSHSecret(args []string) {
	if len(args) != 2 {
		fmt.Printf("Invalid number of arguments, 2 desired, %d given\n", len(args))
	}

	secretName := args[0]
	privateKeyFileName := args[1]

	fileContents, err := ioutil.ReadFile(privateKeyFileName)
	if err != nil {
		fmt.Printf("Could not read in private key file -->%s<--, reason was %s\n", privateKeyFileName, err)
		return
	}
	err = k8sutil.CreateSSHSecret(secretName, viper.GetString("kubernetes-namespace"), fileContents)
	if err != nil {
		fmt.Printf("Could not create secret -->%s<--, reason was %s\n", secretName, err)
	} else {
		fmt.Printf("Was able to create secret -->%s<--\n", secretName)
	}
}
