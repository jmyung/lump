package sshsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
	corev1 "k8s.io/api/core/v1"
)

func init() {
	SSHSecretCmd.AddCommand(generateListSSHSecretCmd())
}

func generateListSSHSecretCmd() *cobra.Command {
	var generateListSSHSecretCmd = &cobra.Command{
		Use:   "list",
		Short: "Lists all " + string(corev1.SecretTypeSSHAuth) + " secrets",
		Long:  "Lists all Kubernetes secrets of type " + string(corev1.SecretTypeSSHAuth),
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			listSSHSecrets(args)
		},
	}

	return generateListSSHSecretCmd
}

func listSSHSecrets(args []string) {
	secrets, err := k8sutil.GetSSHSecretList(viper.GetString("kubernetes-namespace"))
	if err != nil {
		fmt.Printf("Could not list secrets, reason was %s\n", err)
		return
	}
	fmt.Printf("Secrets:\n")
	for _, secret := range secrets {
		fmt.Printf("\t%s\n", secret.Name)
	}
}
