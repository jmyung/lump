package kubeconfigsecrets

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd/k8s"
)

func init() {
	k8s.K8sCmd.AddCommand(KubeconfigSecretCmd)
}

var KubeconfigSecretCmd = generateKubeconfigSecretCmd()

func generateKubeconfigSecretCmd() *cobra.Command {
	var createKubeconfigSecretCmd = &cobra.Command{
		Use:   "kubeconfig-secret",
		Short: "K8S kubeconfig secret commands",
		Long:  `Kubernetes kubeconfig secret Commands`,
	}

	return createKubeconfigSecretCmd
}
