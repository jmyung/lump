package kubeconfigsecrets

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mvenezia/lump/pkg/util/k8sutil"
	corev1 "k8s.io/api/core/v1"
)

func init() {
	KubeconfigSecretCmd.AddCommand(generateGetKubeconfigSecretCmd())
}

func generateGetKubeconfigSecretCmd() *cobra.Command {
	var generateGetKubeconfigSecretCmd = &cobra.Command{
		Use:   "get [secretName]",
		Short: "Get kubeconfig secret",
		Long:  "Get Kubernetes secret of type kubeconfig",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			getKubeconfigSecret(args)
		},
	}

	return generateGetKubeconfigSecretCmd
}

func getKubeconfigSecret(args []string) {
	keyName := args[0]
	secret, err := k8sutil.GetKubeconfigSecret(args[0], viper.GetString("kubernetes-namespace"))
	if err != nil {
		fmt.Printf("Could not get secret %s, reason was %s\n", keyName, err)
		return
	}
	fmt.Printf("Kubeconfig: \n")
	fmt.Printf("%s\n", secret.Data[corev1.ServiceAccountKubeconfigKey])
}
