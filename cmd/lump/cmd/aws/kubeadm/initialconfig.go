package kubeadm

import (
	"fmt"
	yaml2 "github.com/ghodss/yaml"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
	"io/ioutil"
	"os"
)

func init() {
	AWSKubeadmCmd.AddCommand(generateInitialConfigCmd())
}

func generateInitialConfigCmd() *cobra.Command {
	var k8sVersion string
	var usePrivateIPs bool
	var printJSON bool
	var podCIDR string
	var serviceCIDR string
	var dnsDomain string

	var getInitialConfigCmd = &cobra.Command{
		Use:   "initial-config [stackName] [filename]",
		Short: "Returns a kubeadm initial config / master config resource for the cluster in the stack",
		Long:  `Returns a kubeadm initial config / master config resource for the cluster in the stack`,
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createInitialConfig(args, k8sVersion, podCIDR, serviceCIDR, dnsDomain, usePrivateIPs, printJSON)
		},
	}
	getInitialConfigCmd.Flags().StringVar(&k8sVersion, "k8s-version", "1.10.6", "The version of Kubernetes to use")
	getInitialConfigCmd.Flags().StringVar(&podCIDR, "k8s-pod-cidr", "10.244.0.0/16", "The Pod CIDR")
	getInitialConfigCmd.Flags().StringVar(&serviceCIDR, "k8s-service-cidr", "10.96.0.0/16", "The Service CIDR")
	getInitialConfigCmd.Flags().StringVar(&dnsDomain, "k8s-cluster-domain", "cluster.local", "The cluster's domain")
	getInitialConfigCmd.Flags().BoolVar(&usePrivateIPs, "use-private-ips", false, "Whether external or private ips should be used")
	getInitialConfigCmd.Flags().BoolVar(&printJSON, "json", false, "Set to output in JSON instead of YAML")

	return getInitialConfigCmd
}

func createInitialConfig(args []string, k8sVersion string, podCIDR string, serviceCIDR string, dnsDomain string, usePrivateIPs bool, printJSON bool) {
	stack, _, err := awsutil.GetCFStack(args[0])
	if err != nil {
		fmt.Printf("Error retrieving stack: %s", err)
		os.Exit(1)
	}

	data, err := awsutil.GenerateKubeadmInitialConfigHelper(stack, awsutil.KubeadmInitialConfigNonAWSParameters{
		K8sVersion:       k8sVersion,
		PodCIDR:          podCIDR,
		ServiceCIDR:      serviceCIDR,
		DNSDomain:        dnsDomain,
		PrivateAddresses: usePrivateIPs,
		PrintJSON:        printJSON,
	})
	if err != nil {
		fmt.Printf("Error creating master record: %s", err)
		os.Exit(1)
	}
	if !printJSON {
		data, _ = yaml2.JSONToYAML(data)
	}
	if len(args) > 1 {
		// Going to dump the json to a file
		ioutil.WriteFile(args[1], data, 0644)
		fmt.Printf("Wrote to file %s\n", args[1])
		return
	}
	fmt.Printf("%s\n", data)
}
