package aws

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/mvenezia/lump/cmd/lump/cmd"
)

var AWSCmd = generateAWSCmd()

func init() {
	cmd.RootCmd.AddCommand(AWSCmd)

	AWSCmd.PersistentFlags().String("aws-region", "us-east-1", "The AWS region")
	AWSCmd.PersistentFlags().String("aws-access-key-id", "", "The AWS Access Key ID")
	AWSCmd.PersistentFlags().String("aws-secret-access-key", "", "The AWS Secret Access Key")

	viper.BindPFlag("aws-region", AWSCmd.PersistentFlags().Lookup("aws-region"))
	viper.BindPFlag("aws-access-key-id", AWSCmd.PersistentFlags().Lookup("aws-access-key-id"))
	viper.BindPFlag("aws-secret-access-key", AWSCmd.PersistentFlags().Lookup("aws-secret-access-key"))

}

func generateAWSCmd() *cobra.Command {
	var createAWSCmd = &cobra.Command{
		Use:              "aws",
		Short:            "AWS commands",
		Long:             `AWS Commands`,
		TraverseChildren: true,
	}

	return createAWSCmd
}
