package keys

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
	"os"
)

func init() {
	AWSKeysCmd.AddCommand(generateGetKeysCmd())
}

func generateGetKeysCmd() *cobra.Command {
	var getKeysCmd = &cobra.Command{
		Use:   "list",
		Short: "Returns a list of keys",
		Long:  `Find out what ec2 keys are available`,
		Run: func(cmd *cobra.Command, args []string) {
			printAWSKeys()
		},
	}

	return getKeysCmd
}

func printAWSKeys() {
	keyList, err := awsutil.GetKeyList()
	if err != nil {
		fmt.Printf("Error retrieving key list: %s", err)
		os.Exit(1)
	}

	for _, keyItem := range keyList {
		fmt.Printf("%s: %s\n", *keyItem.KeyName, *keyItem.KeyFingerprint)
	}

}
