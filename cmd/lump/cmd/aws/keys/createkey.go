package keys

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
)

func init() {
	AWSKeysCmd.AddCommand(generateCreateKeyCmd())
}

func generateCreateKeyCmd() *cobra.Command {
	var createKeyCmd = &cobra.Command{
		Use:   "create [keyName]",
		Short: "Creates a key",
		Long:  `Creates an AWS KeyPair`,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createAWSKey(args)
		},
	}

	return createKeyCmd
}

func createAWSKey(args []string) {
	for _, keyName := range args {
		privateKey, err := awsutil.CreateKey(keyName)
		if err != nil {
			fmt.Printf("Could not create key -->%s<--, reason was %s\n", keyName, err)
		} else {
			fmt.Printf("Was able to create key -->%s<--, private key is:\n%s\n", keyName, privateKey)
		}
	}
}
