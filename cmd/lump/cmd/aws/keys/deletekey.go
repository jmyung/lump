package keys

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
)

func init() {
	AWSKeysCmd.AddCommand(generateDeleteKeyCmd())
}

func generateDeleteKeyCmd() *cobra.Command {
	var deleteKeyCmd = &cobra.Command{
		Use:   "delete [keyName]",
		Short: "Deletes a key (or more keys)",
		Long:  `Removes an AWS KeyPair`,
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteAWSKey(args)
		},
	}

	return deleteKeyCmd
}

func deleteAWSKey(args []string) {
	for _, keyName := range args {
		err := awsutil.DeleteKey(keyName)
		if err != nil {
			fmt.Printf("Could not delete key -->%s<--, reason was %s\n", keyName, err)
		} else {
			fmt.Printf("Was able to delete key -->%s<--\n", keyName)
		}
	}
}
