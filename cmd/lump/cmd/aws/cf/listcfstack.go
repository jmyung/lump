package cf

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
	"os"
)

func init() {
	AWSCFCmd.AddCommand(generateListCFStacksCmd())
}

func generateListCFStacksCmd() *cobra.Command {
	var getListCFStacksCmd = &cobra.Command{
		Use:   "list",
		Short: "Returns a list of cloud formation stacks",
		Long:  `Find out what AWS Cloud Formation stacks exist`,
		Run: func(cmd *cobra.Command, args []string) {
			printCFStackList()
		},
	}

	return getListCFStacksCmd
}

func printCFStackList() {
	list, err := awsutil.GetCFList()
	if err != nil {
		fmt.Printf("Error retrieving stack list: %s", err)
		os.Exit(1)
	}

	for _, item := range list {
		fmt.Printf("%s: %s\n", *item.StackId, *item.StackName)
	}

}
