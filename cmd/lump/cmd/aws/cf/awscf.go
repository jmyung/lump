package cf

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd/aws"
)

func init() {
	aws.AWSCmd.AddCommand(AWSCFCmd)
}

var AWSCFCmd = generateAWSCFCmd()

func generateAWSCFCmd() *cobra.Command {
	var createAWSCFCmd = &cobra.Command{
		Use:   "cloud-formation",
		Short: "AWS cloud formation commands",
		Long:  `AWS cloud formation Commands`,
	}

	return createAWSCFCmd
}
