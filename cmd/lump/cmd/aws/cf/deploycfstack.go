package cf

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
	"gitlab.com/mvenezia/lump/pkg/aws/models"
)

func init() {
	AWSCFCmd.AddCommand(generateDeployCFStackCmd())
}

func generateDeployCFStackCmd() *cobra.Command {
	var keyPair string
	var username string
	var instanceType string
	var diskSize string
	var availabilityZone string
	var sshAccess string
	var nodeCapacity string

	var createDeployCFStackCmd = &cobra.Command{
		Use:   "deploy [stackName]",
		Short: "Deploys a cloud formation stack",
		Long:  `Deploys an AWS Cloud Formation`,
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deployAWSCFStack(args, keyPair, username, instanceType, diskSize, availabilityZone, sshAccess, nodeCapacity)
		},
	}

	// using standard library "flag" package
	createDeployCFStackCmd.Flags().StringVar(&keyPair, "keypair", "", "The aws keypair to use (required)")
	createDeployCFStackCmd.Flags().StringVar(&username, "username", "ubuntu", "The username to set on instances")
	createDeployCFStackCmd.Flags().StringVar(&instanceType, "instance-type", "m4.large", "The instance type to use")
	createDeployCFStackCmd.Flags().StringVar(&diskSize, "disk-size", "40", "The instance disk size in GB")
	createDeployCFStackCmd.Flags().StringVar(&availabilityZone, "availability-zone", "", "The availability zone to use (required)")
	createDeployCFStackCmd.Flags().StringVar(&sshAccess, "ssh-access", "0.0.0.0/0", "The CIDR that can ssh into instances")
	createDeployCFStackCmd.Flags().StringVar(&nodeCapacity, "node-capacity", "1", "The Node Capacity within K8S")

	createDeployCFStackCmd.MarkFlagRequired("keypair")
	createDeployCFStackCmd.MarkFlagRequired("availability-zone")
	return createDeployCFStackCmd
}

func deployAWSCFStack(args []string, keyPair string, username string, instanceType string, diskSize string, availabilityZone string, sshAccess string, nodeCapacity string) {
	stackName := args[0]
	fmt.Printf("Parsed Information:\n")
	fmt.Printf("\tName: %s\n", stackName)
	fmt.Printf("\tKey Name: %s\n", keyPair)
	fmt.Printf("\tUsername: %s\n", username)
	fmt.Printf("\tInstance Type: %s\n", instanceType)
	fmt.Printf("\tDisk Size in GB: %s\n", diskSize)
	fmt.Printf("\tAvailability Zone: %s\n", availabilityZone)
	fmt.Printf("\tSSH Location: %s\n", sshAccess)
	fmt.Printf("\tK8S Node Capacity: %s\n", nodeCapacity)
	stackId, err := awsutil.DeployCFStack(awsutilmodels.DemoCFTemplateOptions{
		Name:             stackName,
		KeyName:          keyPair,
		Username:         username,
		InstanceType:     instanceType,
		DiskSize:         diskSize,
		AvailabilityZone: availabilityZone,
		SSHLocation:      sshAccess,
		K8sNodeCapacity:  nodeCapacity,
	})
	if err != nil {
		fmt.Printf("Could not create key -->%s<--, reason was %s\n", stackName, err)
		return
	} else {
		fmt.Printf("Was able to create key -->%s<--, stack id is:\n%s\n", stackName, stackId)
	}

	awsutil.WaitUntilStackIsReady(stackName, true)
}
