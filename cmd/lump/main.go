package main

import (
	"gitlab.com/mvenezia/lump/cmd/lump/cmd"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/aws"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/aws/cf"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/aws/clusterapi"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/aws/keys"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/aws/kubeadm"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/cma"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/k8s"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/k8s/kubeconfigsecrets"
	_ "gitlab.com/mvenezia/lump/cmd/lump/cmd/k8s/sshsecrets"
)

func main() {
	cmd.Execute()
}
