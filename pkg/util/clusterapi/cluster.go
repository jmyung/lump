package clusterapiutil

import (
	"gitlab.com/mvenezia/lump/pkg/util/clusterapi/models/v1alpha1"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ClusterAPIVersion = "cluster.k8s.io/v1alpha1"
	ClusterKind       = "Cluster"
	ClusterDefaultPodCIDR = "10.244.0.0/16"
	ClusterDefaultServiceCIDR = "10.244.0.0/16"
	ClusterDefaultServiceDomain = "cluster.local"
)

type ClusterTemplateParameters struct {
	Name   string
}

func GenerateCluster(options ClusterTemplateParameters) v1alpha1.Cluster {
	cluster := v1alpha1.Cluster{
		APIVersion: ClusterAPIVersion,
		Kind:       ClusterKind,
		ObjectMeta: v1.ObjectMeta{
			Name: options.Name,
		},
		Spec: v1alpha1.ClusterSpec{
			ClusterNetwork: v1alpha1.ClusterNetworkingConfig{
				Services: v1alpha1.NetworkRanges{
					CIDRBlocks: []string{ClusterDefaultServiceCIDR},
				},
				Pods: v1alpha1.NetworkRanges{
					CIDRBlocks: []string{ClusterDefaultPodCIDR},
				},
				ServiceDomain: ClusterDefaultServiceDomain,
			},
		},
	}
	return cluster
}
