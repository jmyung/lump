package k8sutil

import (
	"io"
	podv1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

func GetLogsForPod(namespace string, pod string, outputText chan string, outputControl chan string) (err error) {
	if DefaultConfig == nil {
		DefaultConfig, _ = GenerateKubernetesConfig()
	}
	client, err := kubernetes.NewForConfig(DefaultConfig)
	if err != nil {
		return
	}
	sinceSeconds := int64(60)
	log := client.CoreV1().Pods(namespace).GetLogs(pod, &podv1.PodLogOptions{Follow: true, SinceSeconds: &sinceSeconds})
	readCloser, err := log.Stream()
	if err != nil {
		return
	}

	go streamLog(readCloser, outputText, outputControl)
	return
}

func streamLog(reader io.Reader, input chan string, control <-chan string) {
	p := make([]byte, 65536)
	for {
		select {
		case _ = <-control:
			return
		default:
			n, err := reader.Read(p)
			if err != nil {
				if err == io.EOF {
					return
				}
			}
			input <- string(p[:n])
			logger.Infof("Message was: %s", string(p[:n]))
		}
	}
}
